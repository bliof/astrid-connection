#         FILE: Utils.pm
#
#  DESCRIPTION:
#
#        FILES: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Aleksandar Ivanov, aivanov92@gmail.com
# ORGANIZATION:
#      VERSION: 0.1
#      CREATED: 2012-12-30 21:42
#     REVISION: ---
#===============================================================================

package Astrid::Test::Utils;

use strict;
use warnings;
use File::Spec;

use Exporter;

our @ISA = ('Exporter');
our @EXPORT = qw(
    t_connection
    $t_config
);

use YAML qw(LoadFile);
use Astrid::Connection;
use File::Spec;

our $t_config;
our $t_config_file;

sub _require_option {
    my $option = shift;

    unless ($t_config->{$option}) {
        die "No '$option' specified in the config [$t_config_file]";
    }
}

BEGIN {
    my ($volume, $dir, $file) = File::Spec->splitpath( __FILE__ );

    $t_config_file = File::Spec->catfile($dir, ('..') x 3, 'config.yaml');

    eval {
        $t_config = LoadFile($t_config_file);
    };

    if ($@) {
        die "Cannot load config file [$t_config_file]";
    }

    _require_option($_) for qw(app_id secret);
}

sub t_connection {
    my %astrid = (
        app_id => $t_config->{app_id},
        secret => $t_config->{secret},
    );
    $astrid{server} = $t_config->{server} if $t_config->{server};

    return Astrid::Connection->new(
        %astrid,
        @_
    );
}

1;

