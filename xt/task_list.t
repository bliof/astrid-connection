#!/usr/bin/env perl
#===============================================================================
#
#         FILE: task_list.t
#
#  DESCRIPTION:
#
#        FILES: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Aleksandar Ivanov, aivanov92@gmail.com
# ORGANIZATION:
#      VERSION: 0.1
#      CREATED: 2013-02-12 23:15
#     REVISION: ---
#===============================================================================

use strict;
use warnings;

use Test::More;

use Astrid::Test::Utils;

my $astrid = t_connection();

my $login = $astrid->sign_in($t_config->{user}->{email}, $t_config->{user}->{password});

if ($login->{status} ne 'success') {
    BAIL_OUT "Cannot login to astrid";
}

my $result = $astrid->task_list();

diag explain $result;
cmp_ok($result->{status}, 'eq', 'success', 'tasks fetched');

done_testing;


