#!/usr/bin/env perl
#===============================================================================
#
#         FILE: sign_in.t
#
#  DESCRIPTION:
#
#        FILES: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Aleksandar Ivanov, aivanov92@gmail.com
# ORGANIZATION:
#      VERSION: 0.1
#      CREATED: 2013-01-03 21:58
#     REVISION: ---
#===============================================================================

use strict;
use warnings;

use Test::More;
use Astrid::Test::Utils;

my $astrid = t_connection();

my ($result, $response) = $astrid->sign_in($t_config->{user}->{email}, $t_config->{user}->{password});

note explain $result;
cmp_ok($result->{status}, 'eq', 'success');

done_testing();
