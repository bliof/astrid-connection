#!/usr/bin/env perl
#===============================================================================
#
#         FILE: basics.t
#
#  DESCRIPTION:
#
#        FILES: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Aleksandar Ivanov, aivanov92@gmail.com
# ORGANIZATION:
#      VERSION: 0.1
#      CREATED: 2012-12-30 21:06
#     REVISION: ---
#===============================================================================

use strict;
use warnings;

use Test::More;
use Astrid::Connection;

BEGIN {
    my $astrid = new_ok('Astrid::Connection', [secret => '1234', app_id => 'ninja']);

    unless ($astrid) {
        BAIL_OUT('**** Cannot create an Astrid::Connection ****');
    }

    cmp_ok($astrid->secret(), 'eq', '1234');
    cmp_ok($astrid->app_id(), 'eq', 'ninja');
    cmp_ok($astrid->api_version(), 'eq', '7');
    cmp_ok($astrid->server(), 'eq', 'https://astrid.com');
}

done_testing;

