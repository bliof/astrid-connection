#===============================================================================
#
#         FILE: Connection.pm
#
#  DESCRIPTION:
#
#        FILES: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Aleksandar Ivanov, aivanov92@gmail.com
# ORGANIZATION:
#      VERSION: 0.1
#      CREATED: 2012-12-30 16:18
#     REVISION: ---
#===============================================================================

package Astrid::Connection;

use strict;
use warnings;
use Object::Tiny qw(
    secret
    app_id
    api_version
    server
    user
    token
);
use Digest::MD5 qw/md5_hex/;
use LWP::UserAgent;
use JSON qw/decode_json/;

sub new {
    my $class = shift;
    my $self = {
        api_version => 7,
        server => 'https://astrid.com',
        @_
    };

    $self->{ua} = LWP::UserAgent->new();

    bless ($self, $class);
    return $self;
}

sub sign_in {
    my $self = shift;
    my $email = shift;
    my $password = shift;

    my ($result, $response) = $self->user_signin(
        email => $email,
        secret => $password,
        provider => 'password'
    );

    $self->{token} = $result->{token};
    $self->{user} = $result;

    return wantarray ? ($result, $response) : $result;
}

sub send_request {
    my $self = shift;
    my $method = shift;
    my $data = shift || {};

    my $url = $self->server . '/api/' . $self->api_version . '/' . $method;
    my $params = $self->build_params($method => $data);

    my $response = $self->{ua}->post($url => $params);

    #FIXME there could be some magical conditions...
    my $result = decode_json($response->content);

    return wantarray ? ($result, $response) : $result;
}

sub build_params {
    my $self = shift;
    my $method = shift;
    my $data = shift || {};

    my $params = {
        time   => time,
        app_id => $self->{app_id},
        map { ((ref($data->{$_}) eq 'ARRAY') ? _fix_array_param_name($_) : $_) => $data->{$_} }
            keys %$data
    };

    if (!$data->{token} and $self->{token}) {
        $params->{token} = $self->{token};
    }

    my $md5 = $self->generate_md5($method => $params);

    $params->{sig} = $md5;

    return $params;
}

sub _fix_array_param_name {
    my $param = shift;
    $param =~ s/(\[\])?$/[]/;
    return $param;
}

sub generate_md5 {
    my $self = shift;
    my $method = shift;
    my $params = shift || {};

    my $data = $method;
    for my $id (sort keys %$params) {
        my $value = $params->{$id};

        no warnings 'uninitialized';
        if (ref($value) eq 'ARRAY') {
            $data .= $id . $_ for @$value;
        } else {
            $data .= $id . $value;
        }
    }

    $data .= $self->{secret};

    return md5_hex($data);
}

#===============================================================================
# Astrid api - version 7
#===============================================================================

sub user_signin             { shift->send_request('user_signin',            { @_ }) }
sub user_save               { shift->send_request('user_save',              { @_ }) }
sub user_add_auth           { shift->send_request('user_add_auth',          { @_ }) }
sub user_invite             { shift->send_request('user_invite',            { @_ }) }
sub user_set_status         { shift->send_request('user_set_status',        { @_ }) }
sub user_reset_password     { shift->send_request('user_reset_password',    { @_ }) }
sub user_status             { shift->send_request('user_status',            { @_ }) }
sub users_exist             { shift->send_request('users_exist',            { @_ }) }
sub task_save               { shift->send_request('task_save',              { @_ }) }
sub task_show               { shift->send_request('task_show',              { @_ }) }
sub task_set_following      { shift->send_request('task_set_following',     { @_ }) }
sub task_update_people      { shift->send_request('task_update_people',     { @_ }) }
sub task_share_social       { shift->send_request('task_share_social',      { @_ }) }
sub task_list               { shift->send_request('task_list',              { @_ }) }
sub list_order              { shift->send_request('list_order',             { @_ }) }
sub task_attachment_create  { shift->send_request('task_attachment_create', { @_ }) }
sub task_attachment_remove  { shift->send_request('task_attachment_remove', { @_ }) }
sub task_attachment_list    { shift->send_request('task_attachment_list',   { @_ }) }
sub list_save               { shift->send_request('list_save',              { @_ }) }
sub list_show               { shift->send_request('list_show',              { @_ }) }
sub list_list               { shift->send_request('list_list',              { @_ }) }
sub tag_save                { shift->send_request('tag_save',               { @_ }) }
sub tag_show                { shift->send_request('tag_show',               { @_ }) }
sub tag_list                { shift->send_request('tag_list',               { @_ }) }
sub activity_list           { shift->send_request('activity_list',          { @_ }) }
sub comment_add             { shift->send_request('comment_add',            { @_ }) }
sub user_show               { shift->send_request('user_show',              { @_ }) }
sub user_list               { shift->send_request('user_list',              { @_ }) }
sub featured_lists          { shift->send_request('featured_lists',         { @_ }) }
sub featured_list_subscribe { shift->send_request('featured_list_subscribe',{ @_ }) }
sub sync                    { shift->send_request('sync',                   { @_ }) }
sub time                    { shift->send_request('time',                   { @_ }) }

1;

